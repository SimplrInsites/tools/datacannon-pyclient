# Data Cannon PyClient

Extension for the Data Cannon Client using Python. Attempts to link to the client and have Python functions
pass serialized tasks to the app for publishing to the chosen Broker.

Will include:
 - Configuration Creator in Python
 - Task Builder in Python
 - Stream creator in Python
 - Functions to connect to an underlying core application using tokio a mpsc channel
 
The initial use is for attaching Python aggregation systems to ETL data pipelines built using Rust to avoid
issues with the GIL. The GIL results in a much larger resource footprint while slowing many NLP and ETL tasks
to a crawl compared to langauges that are not forcibly single threaded or forcibly multiprocessed. Threads
will always use less memory and offer advantages over multi-processing. Both of these benefits are necessary
when loading large data models into RAM for ETL.

*Tim:* This is how I will be moving towards build as you go instead of building a framework and then working
on data pipelines :)

This is a work in progress.